/*
 * virsh-snapshot.h: Commands to manage domain snapshot
 *
 * Copyright (C) 2005, 2007-2012 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 *  Daniel Veillard <veillard@redhat.com>
 *  Karel Zak <kzak@redhat.com>
 *  Daniel P. Berrange <berrange@redhat.com>
 *
 */

#ifndef VIRSH_SNAPSHOT_H
# define VIRSH_SNAPSHOT_H

# include "virsh.h"

extern const vshCmdDef snapshotCmds[];

/*
 * snapshot operation/stages
 */

#define SNAPSHOT_NONE 0             /* No distributed snapshot is going on. */
#define SNAPSHOT_STARTED 1            /* Local snapshot is being taking. */
#define SNAPSHOT_COMPLETED 2       /* Local snapshot finishes. */
#define SNAPSHOT_ALL_COMPLETED 3      /* Global snapshot finishes. */
#define SNAPSHOT_STAGE_QUERY 4      /* query snapshot stages. */

#endif /* VIRSH_SNAPSHOT_H */
